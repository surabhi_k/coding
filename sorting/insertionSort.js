var arr = [9, 6, 4, 2, 7, 5];

function insertionSort(a) {
  for(var i = 1; i < a.length; i++) {
    let key = a[i];
    let j = i-1;
    while(j >= 0 && key < a[j]) {
      a[j+1] = a[j];
      j = j-1;      
    }
    a[j+1] = key;
  }
}

console.log("BEFORE SORTING", arr);
insertionSort(arr);
console.log("AFTER SORTING", arr);
