function SelectionSort(arr) {
  for(var i = 0; i < arr.length; i++) {
    let min = i;
    for(var j = i+1; j < arr.length; j++) {
      if(arr[min] > arr[j]) {
        min = j;
      }
    }

    // Swap min i
    let temp = arr[i];
    arr[i] = arr[min];
    arr[min] = temp;
  }
}

var Arr = [10, 5, 7, 8, 3];

console.log("Before Sorting", Arr);
SelectionSort(Arr);
console.log("After Sorting", Arr);
